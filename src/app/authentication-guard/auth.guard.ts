import { Injectable } from "@angular/core";
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs/Observable";

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router) { }
    public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
        : Observable<boolean> | Promise<boolean> | boolean {
        console.log("auth guard" + localStorage.getItem('currentUser'));
        if (localStorage.getItem('currentUser')) {
            console.log("Authguard return true");
            return true;
        } else {
            console.log("Auth guard returned false");
            this.router.navigate(['']);
            return false;
        }

    }
}   