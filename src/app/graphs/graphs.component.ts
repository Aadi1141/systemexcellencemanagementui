import { Component, OnInit } from '@angular/core';
import { PredictedResults } from '../modal/predictedResult.modal';
import { Chart, ChartDataSets, ChartOptions } from 'chart.js';
import { Label, Color } from 'ng2-charts';

@Component({
  selector: 'app-graphs',
  templateUrl: './graphs.component.html',
  styleUrls: ['./graphs.component.css']
})
export class GraphsComponent implements OnInit {

  predictedResults: PredictedResults;
  lineChartData: number[] = [];
  drfChartData: number[] = [];
  chartData: any;
  colors: any;
  labels: any;
  chartOptions: any;
  drfData: any;
  drfLabels: any;
  drfOptions: any;
  testChartData: number[] = [];
	onChartClick:any;
  
  constructor() { }

  ngOnInit() {
    this.predictedResults = JSON.parse(localStorage.getItem('graphData'));
    this.gbmPerformanceGraph();
    this.drfPerformanceGraph();
    this.drfVariableImportanceGraph();
    this.gbmVariableImportanceGraph();
    this.testGraph();
    this.trainGraph();
    this.trainGraph1();
  }
  gbmPerformanceGraph() {
    this.lineChartData = JSON.parse(this.predictedResults.top_model1_performance_mat);
    var labels = [];
    for (var i = 0; i < this.lineChartData.length; i++) {
      labels.push(i);
    }
    var ctx = (document.getElementById('gbm') as HTMLCanvasElement).getContext("2d");
    var myChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: labels,
        datasets: [{
          label: "GBM Model Performance",
          borderColor: "#80b6f4",
          pointBorderColor: "#80b6f4",
          pointBackgroundColor: "#80b6f4",
          pointHoverBackgroundColor: "#80b6f4",
          pointHoverBorderColor: "#80b6f4",
          pointBorderWidth: 10,
          pointHoverRadius: 10,
          pointHoverBorderWidth: 1,
          pointRadius: 0,
          fill: false,
          borderWidth: 4,
          data: this.lineChartData
        }]
      },
      options: {
        legend: {
          position: "bottom"
        },
        scales: {
          yAxes: [{
            ticks: {
              fontColor: "rgba(0,0,0,0.5)",
              fontStyle: "bold",
              beginAtZero: true,
              //maxTicksLimit: 5,
              padding: 20,
              stepSize: 10,
              max: 100
            },
            scaleLabel: {
              display: true,
              labelString: 'RMSE'
            },
            gridLines: {
              drawTicks: false,
              display: false
            }
          }],
          xAxes: [{
            gridLines: {
              zeroLineColor: "transparent"
            },
            ticks: {
              padding: 40,
              fontColor: "rgba(0,0,0,0.5)",
              fontStyle: "bold",
              beginAtZero: true,
              max: 300
            }
            , scaleLabel: {
              display: true,
              labelString: 'Number of trees'
            }
          }]
        }
      }
    });
  }
  drfPerformanceGraph() {
    this.drfChartData = JSON.parse(this.predictedResults.top_model2_performance_mat);
    var drfLables = [];
    for (var i = 0; i < this.drfChartData.length; i++) {
      drfLables.push(i);
    }
    var ctx1 = (document.getElementById('drf') as HTMLCanvasElement).getContext("2d");
    var myChart = new Chart(ctx1, {
      type: 'line',
      data: {
        labels: drfLables,
        datasets: [{
          label: "Random Forest Model Performance",
          borderColor: "#80b6f4",
          pointBorderColor: "#80b6f4",
          pointBackgroundColor: "#80b6f4",
          pointHoverBackgroundColor: "#80b6f4",
          pointHoverBorderColor: "#80b6f4",
          pointBorderWidth: 10,
          pointHoverRadius: 10,
          pointHoverBorderWidth: 1,
          pointRadius: 0,
          fill: false,
          borderWidth: 4,
          data: this.drfChartData
        }]
      },
      options: {
        legend: {
          position: "bottom"
        },
        scales: {
          yAxes: [{
            ticks: {
              fontColor: "rgba(0,0,0,0.5)",
              fontStyle: "bold",
              beginAtZero: true,
              //maxTicksLimit: 5,
              padding: 20,
              stepSize: 10,
              max: 100
            },
            scaleLabel: {
              display: true,
              labelString: 'RMSE'
            },
            gridLines: {
              drawTicks: false,
              display: false
            }
          }],
          xAxes: [{
            gridLines: {
              zeroLineColor: "transparent"
            },
            ticks: {
              padding: 40,
              fontColor: "rgba(0,0,0,0.5)",
              fontStyle: "bold",
              beginAtZero: true,
              max: 300
            }
            , scaleLabel: {
              display: true,
              labelString: 'Number of trees'
            }
          }]
        }
      }
    });
  }

  gbmVariableImportanceGraph() {
    const gradient = (document.getElementById('gbmCanvas') as HTMLCanvasElement).getContext('2d').createLinearGradient(0, 0, 0, 200);
    gradient.addColorStop(0, 'blue');
    gradient.addColorStop(1, '#95daf9');
    var data: number[] = [];
    data = this.predictedResults.top_model1_var_imp;
    this.chartData = [{
      label: 'Score',
      data: data
    }];

    this.colors = [
      {
        backgroundColor: gradient
      }
    ]
    this.labels = this.predictedResults.top_model1_variable;
    this.chartOptions = {
      responsive: true,
      scales: {
        yAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'Variable Importance'
          }
        }
        ],
        xAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'Variable'
          }
        }]
      }
    }
  }
  drfVariableImportanceGraph() {
    const gradient = (document.getElementById('drfCanvas') as HTMLCanvasElement).getContext('2d').createLinearGradient(0, 0, 0, 200);
    gradient.addColorStop(0, 'blue');
    gradient.addColorStop(1, '#95daf9');
    var data: number[] = [];
    data = this.predictedResults.top_model2_var_imp;
    this.drfData = [{
      label: 'Score',
      data: data
    }];

    this.colors = [
      {
        backgroundColor: gradient
      }
    ]
    this.drfLabels = this.predictedResults.top_model2_variable;
    this.drfOptions = {
      responsive: true,
      scales: {
        yAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'Variable Importance'
          }
        }
        ],
        xAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'Variable'
          }
        }]
      }
    }
  }
  testGraph() {
    var testChartData1 = JSON.parse(this.predictedResults.actual_rul);
    var testChartData2 = JSON.parse(this.predictedResults.predicted_rul);
    var labels = [];
    for (var i = 0; i < testChartData1.length; i++) {
      labels.push(i);
    }
    var ctx = (document.getElementById('testCanvas') as HTMLCanvasElement).getContext("2d");
    var myChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: labels,
        datasets: [{
          label: "Actual Remaining Useful Life",
          borderColor: "#f49242",
          pointBorderColor: "#f49242",
          pointBackgroundColor: "#f49242",
          pointHoverBackgroundColor: "#f49242",
          pointHoverBorderColor: "#f49242",
          pointBorderWidth: 10,
          pointHoverRadius: 10,
          pointHoverBorderWidth: 1,
          pointRadius: 2,
          fill: false,
          borderWidth: 2,
          borderDash: [10, 5],
          data: testChartData1
        },
        {
          label: "Predicted Remaining Useful Life",
          borderColor: "#002259",
          pointBorderColor: "#002259",
          pointBackgroundColor: "#002259",
          pointHoverBackgroundColor: "#002259",
          pointHoverBorderColor: "#002259",
          pointBorderWidth: 10,
          pointHoverRadius: 10,
          pointHoverBorderWidth: 1,
          pointRadius: 2,
          fill: false,
          borderWidth: 2,
          //borderDash: [10,5],
          data: testChartData2
        }
        ]
      },
      options: {
        legend: {
          position: "top"
        },
        scales: {
          yAxes: [{
            ticks: {
              fontColor: "rgba(0,0,0,0.5)",
              fontStyle: "bold",
              beginAtZero: true,
              //maxTicksLimit: 5,
              padding: 20,
              stepSize: 20,
              max: 250
            },
            scaleLabel: {
              display: true,
              labelString: 'Estimated RUL (In cycles)'
            },
            gridLines: {
              drawTicks: false,
              display: false
            }
          }],
          xAxes: [{
            gridLines: {
              zeroLineColor: "transparent"
            },
            ticks: {
              padding: 40,
              fontColor: "rgba(0,0,0,0.5)",
              fontStyle: "bold",
              beginAtZero: true,
              max: 300
            }
            , scaleLabel: {
              display: true,
              labelString: 'Unit Number'
            }
          }]
        }
      }
    });
  }

  trainGraph() {
    var testChartData1 = this.predictedResults.train_predict_drf;
    var testChartData2 = this.predictedResults.train_predict_gbm;
    var rul = this.predictedResults.train_RemainingUsefulLife;

    var labels = [];
    for (var i = 0; i < testChartData1.length; i++) {
      labels.push(i);
    }
    var ctx = (document.getElementById('trainCanvas') as HTMLCanvasElement).getContext("2d");
    var myChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: labels,
        datasets: [{
          label: "Predicted DRF",
          borderColor: "#f49242",
          pointBorderColor: "#f49242",
          pointBackgroundColor: "#f49242",
          pointHoverBackgroundColor: "#f49242",
          pointHoverBorderColor: "#f49242",
          pointBorderWidth: 1,
          pointHoverRadius: 1,
          pointHoverBorderWidth: 1,
          pointRadius: 2,
          fill: false,
          borderWidth: 2,
          borderDash: [10, 5],
          data: testChartData1
        },
        {
          label: "Predicted GBM",
          borderColor: "#002259",
          pointBorderColor: "#002259",
          pointBackgroundColor: "#002259",
          pointHoverBackgroundColor: "#002259",
          pointHoverBorderColor: "#002259",
          pointBorderWidth: 1,
          pointHoverRadius: 1,
          pointHoverBorderWidth: 1,
          pointRadius: 2,
          fill: false,
          borderWidth: 2,
          //borderDash: [10,5],
          data: testChartData2
        },
        {
          label: "Remaining Useful Life",
          borderColor: "green",
          pointBorderColor: "green",
          pointBackgroundColor: "green",
          pointHoverBackgroundColor: "green",
          pointHoverBorderColor: "green",
          pointBorderWidth: 2,
          pointHoverRadius: 2,
          pointHoverBorderWidth: 1,
          pointRadius: 2,
          fill: false,
          borderWidth: 2,
          borderDash: [5, 2],
          data: rul
        }
        ]
      },
      options: {
        legend: {
          position: "top"
        },
        scales: {
          yAxes: [{
            ticks: {
              fontColor: "rgba(0,0,0,0.5)",
              fontStyle: "bold",
              beginAtZero: true,
              //maxTicksLimit: 5,
              padding: 20,
              stepSize: 100,
              max: 400
            },
            scaleLabel: {
              display: true,
              labelString: 'Estimated RUL (In cycles)'
            },
            gridLines: {
              drawTicks: false,
              display: false
            }
          }],
          xAxes: [{
            gridLines: {
              zeroLineColor: "transparent"
            },
            ticks: {
              padding: 40,
              fontColor: "rgba(0,0,0,0.5)",
              fontStyle: "bold",
              beginAtZero: true,
              max: 5000
            }
            , scaleLabel: {
              display: true,
              labelString: 'Operational Cycles'
            }
          }]
        }
      }
    });
  }

  trainGraph1() {
    var testChartData1 = this.predictedResults.train_graph_drf;
    var testChartData2 = this.predictedResults.train_graph_gbm;
    var rul = this.predictedResults.train_graph_rul;

    var labels = [];
    for (var i = 0; i < testChartData1.length; i++) {
      labels.push(i);
    }
    var ctx = (document.getElementById('trainCanvas1') as HTMLCanvasElement).getContext("2d");
    var myChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: labels,
        datasets: [{
          label: "Predicted DRF",
          borderColor: "#f49242",
          pointBorderColor: "#f49242",
          pointBackgroundColor: "#f49242",
          pointHoverBackgroundColor: "#f49242",
          pointHoverBorderColor: "#f49242",
          pointBorderWidth: 1,
          pointHoverRadius: 1,
          pointHoverBorderWidth: 1,
          pointRadius: 2,
          fill: false,
          borderWidth: 2,
          borderDash: [10, 5],
          data: testChartData1
        },
        {
          label: "Predicted GBM",
          borderColor: "#002259",
          pointBorderColor: "#002259",
          pointBackgroundColor: "#002259",
          pointHoverBackgroundColor: "#002259",
          pointHoverBorderColor: "#002259",
          pointBorderWidth: 1,
          pointHoverRadius: 1,
          pointHoverBorderWidth: 1,
          pointRadius: 2,
          fill: false,
          borderWidth: 2,
          //borderDash: [10,5],
          data: testChartData2
        },
        {
          label: "Remaining Useful Life",
          borderColor: "green",
          pointBorderColor: "green",
          pointBackgroundColor: "green",
          pointHoverBackgroundColor: "green",
          pointHoverBorderColor: "green",
          pointBorderWidth: 2,
          pointHoverRadius: 2,
          pointHoverBorderWidth: 1,
          pointRadius: 2,
          fill: false,
          borderWidth: 2,
          borderDash: [5, 2],
          data: rul
        }
        ]
      },
      options: {
        legend: {
          position: "top"
        },
        scales: {
          yAxes: [{
            ticks: {
              fontColor: "rgba(0,0,0,0.5)",
              fontStyle: "bold",
              beginAtZero: true,
              //maxTicksLimit: 5,
              padding: 20,
              stepSize: 100,
              max: 400
            },
            scaleLabel: {
              display: true,
              labelString: 'Estimated RUL (In cycles)'
            },
            gridLines: {
              drawTicks: false,
              display: false
            }
          }],
          xAxes: [{
            gridLines: {
              zeroLineColor: "transparent"
            },
            ticks: {
              padding: 40,
              fontColor: "rgba(0,0,0,0.5)",
              fontStyle: "bold",
              beginAtZero: true,
              max: 30000
            }
            , scaleLabel: {
              display: true,
              labelString: 'Operational Cycles'
            }
          }]
        }
      }
    });
  }


}
