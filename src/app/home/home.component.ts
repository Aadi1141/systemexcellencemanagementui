import { Component, OnInit } from '@angular/core';
import { GetAnalysisService } from '../services/getAnalysis.service';
import { PredictedResults } from '../modal/predictedResult.modal';
import { Router } from '@angular/router';
import { AnomalyDetectionModel } from '../modal/anomalyDetection.model';
import { TooltipPosition } from '@angular/material';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  predictedResults: PredictedResults;
  actual_rul;
  predicted_rul;
  score
  unitNumber;
  showSpinner = true;
  showData = false;
  showPredictedData = false;
  showButton = false;
  showButton2 = false;
  showRul: boolean = true;
  adDta: AnomalyDetectionModel;
  showAnomalyData: boolean = false;
  showAnomalySpinner: boolean = false;
  data = [];
  xElement = [];
  data1 = [];
  xElement1 = [];
  displayedColumns: string[] = ['Hybrid Rank Score', 'Feature Name', 'Index'];
  dataSource: any;

  constructor(private getAnalysisService: GetAnalysisService,
    private router: Router) { }

  ngOnInit() {

    this.predictedResults = this.getAnalysisService.getPredictedResultObject();
    if (JSON.parse(localStorage.getItem('fanData'))) {
      this.predictedResults = JSON.parse(localStorage.getItem('fanData'));
      this.showButton = true;
    }
    if (JSON.parse(localStorage.getItem('graphData'))) {
      this.predictedResults = JSON.parse(localStorage.getItem('graphData'));
      this.showButton2 = true;
    }
  }

  runAnalysis1() {
    this.showPredictedData = true
    this.showSpinner = true;
    this.showData = false;
    this.getAnalysisService.getAnalysisModel1().subscribe(data => {
      this.predictedResults = <PredictedResults>data;
      this.actual_rul = JSON.parse(this.predictedResults.actual_rul);
      this.predicted_rul = JSON.parse(this.predictedResults.predicted_rul);
      this.score = JSON.parse(this.predictedResults.score);
      this.unitNumber = JSON.parse(this.predictedResults.unitNumber);
      this.showSpinner = false;
      this.showData = true;
      this.showButton = true;
      localStorage.setItem('fanData', JSON.stringify(this.predictedResults));
    })
  }


  runAnalysis2() {
    this.showPredictedData = true;
    this.showSpinner = true;
    this.showData = false;
    this.getAnalysisService.getAnalysisModel2().subscribe(data => {
      this.predictedResults = <PredictedResults>data;
      this.actual_rul = JSON.parse(this.predictedResults.actual_rul);
      this.predicted_rul = JSON.parse(this.predictedResults.predicted_rul);
      this.score = JSON.parse(this.predictedResults.score);
      this.unitNumber = JSON.parse(this.predictedResults.unitNumber);
      this.showSpinner = false;
      this.showData = true;
      this.showButton2 = true;
      localStorage.setItem('graphData', JSON.stringify(this.predictedResults));
    })
  }

  viewGraph() {
    this.router.navigate(['graphs']);
  }

  viewFanGraph() {
    this.router.navigate(['fan_graphs']);
  }
  counter = 0;
  showAdGraph = false;
  runAnomalyDetectionModel() {
    this.showAnomalySpinner = true;
    this.getAnalysisService.getAnomalyDetectionModel().subscribe(
      data => {
        this.adDta = <AnomalyDetectionModel>data;
        localStorage.setItem('adDta', JSON.stringify(this.adDta));
        for (var i = 0; i < 1000; i++) {
          this.xElement.push(i);
          this.xElement.push(this.adDta.conv_reconstruction_error[i]);
          this.xElement.push(this.adDta.model2_threshold);
          if (this.data.push(this.xElement)) {
            this.xElement = [];
          }
        }
        for (var i = 0; i < 1000; i++) {
          this.xElement1.push(i);
          this.xElement1.push(this.adDta.feedForward_reconstruction_error[i]);
          this.xElement1.push(this.adDta.model1_threshold);
          if (this.data1.push(this.xElement1)) {
            this.xElement1 = [];
          }
        }
        console.log("data ua ", JSON.stringify(this.data1));
        this.showAnomalyData = true;
        this.showAnomalySpinner = false;
        this.showAdGraph = true;
      }
    )
  }

  title = 'Anomaly Detection on NASA Jet Engine Fleet Using Convolutional-1D Autoencoder';
  type = 'ScatterChart';

  columnNames = ['Data Point Index', 'Reconstruction Error', 'Threshold'];
  options = {
    explorer: {
      actions: ['dragToZoom', 'rightClickToReset'],
      axis: 'horizontal',
      keepInBounds: true,
      maxZoomIn: 24.0,
      maxZoomOut: 0.1,
    },
    series: {
      0: { type: 'line', lineWidth: 1 }
    },
    pointSize: 1,
    animation: {
      duration: 1000,
      easing: 'linear',
      startup: true
    },
    hAxis: {
      title: 'Data Point Index',
    }
  };


  title1 = 'Anomaly Detection on NASA Jet Engine Fleet Using Feedforward Autoencoder';
  type1 = 'ScatterChart';

  columnNames1 = ['Data Point Index', 'Reconstruction Error', 'Threshold'];
  options1 = {
    explorer: {
      actions: ['dragToZoom', 'rightClickToReset'],
      axis: 'horizontal',
      keepInBounds: true,
      maxZoomIn: 24.0,
      maxZoomOut: 0.1,
    },
    animation: {
      duration: 1000,
      easing: 'linear',
      startup: true
    },
    pointSize: 1,
    series: {
      0: { type: 'line', lineWidth: 1 }
    },
    hAxis: {
      title: 'Data Point Index',
    },
    vAxis: {
      title: 'Reconstruction Error',
    }

  };

  showGraph() {
    this.router.navigate(['/ad_graphs']);
  }
}
