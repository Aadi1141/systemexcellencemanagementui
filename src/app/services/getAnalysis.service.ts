import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import { PredictedResults } from '../modal/predictedResult.modal';
import { environment } from 'src/environments/environment';
import { AnomalyDetectionModel } from '../modal/anomalyDetection.model';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class GetAnalysisService {

    private predictModelUrl = environment.url + ":8701/fan_predict";
    //private predictModelUrl = 'http://localhost:8701/fan_predict';
    private predictModelUrl2 = environment.url + ":8701/fan_hpc_predict";
    private anomalyDetectionUrl = environment.url + ":9000/ad_turbofan_predict";
    //private predictModelUrl2 = "http://localhost:8701/fan_hpc_predict";
    constructor(private http: HttpClient) { }

    getAnalysisModel1(): Observable<any> {
        return this.http.get<any>(this.predictModelUrl).pipe(
            tap(data => console.log(``)),
            catchError(this.handleError<any>(``))
        );
    }

    getAnalysisModel2(): Observable<any> {
        return this.http.get<any>(this.predictModelUrl2).pipe(
            tap(data => console.log(``)),
            catchError(this.handleError<any>(``))
        );
    }

    getAnomalyDetectionModel(): Observable<any> {
        return this.http.get<any>(this.anomalyDetectionUrl).pipe(
            tap(data => console.log(``)),
            catchError(this.handleError<any>(``))
        );
    }

    /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            //console.error(error); // log to console instead
            // TODO: better job of transforming error for user consumption
            //console.log(`${operation} failed: ${error.message}`);
            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    getPredictedResultObject() {
        var predictedResult: PredictedResults = {
            actual_rul: [],
            predicted_rul: [],
            unitNumber: [],
            score: []
        } as any;
        return predictedResult;
    }

    getAdObject() {
        var adObj: AnomalyDetectionModel = {
            conv_feature_name: [],
            conv_hybrid_rank_score: [],
            conv_index: [],
            conv_reconstruction_error: [],
            feedForward_feature_name: [],
            feedForward_hybrid_rank_score: [],
            feedForward_index: [],
            feedForward_reconstruction_error: [],
            model1_threshold: null,
            model2_threshold: null,
        } as any;
        return adObj;
    }
}

