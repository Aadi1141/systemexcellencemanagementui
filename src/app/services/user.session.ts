import { Injectable } from "@angular/core";

@Injectable()
export class UserSessionService {
    private _session: Object;
    constructor() { }
    setSession(value) {
        this._session = value;
    }
    getSession() {
        return this._session;
    }
}