import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { LogIn } from '../modal/login.modal';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class LoginService {

    private loginUrl = environment.url1 + ':3002/logIn';
    constructor(private http: HttpClient) { }
    login(input: any): Observable<LogIn> {
        console.log("url is ", this.loginUrl);

        return this.http.post<LogIn>(this.loginUrl, input, httpOptions).pipe(
            tap(_ => console.log("")),
            catchError(this.handleError<any>(''))
        );
    }

    /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            //console.error(error); // log to console instead
            // TODO: better job of transforming error for user consumption
            // console.log(`${operation} failed: ${error.message}`);
            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    getLoginModel() {
        var loginModel: LogIn = {
            username: '',
            password: ''
        } as any;
        return loginModel;
    }
}

