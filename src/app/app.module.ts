import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatCardModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatTabsModule,
  MatMenuModule,
  MatIconModule,
  MatStepperModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatChipsModule,
  MatExpansionModule,
  MatListModule,
  MatTableModule,
  MatProgressSpinnerModule,
  MatAutocompleteModule,
  MatDialogModule,
  MatSnackBarModule,
  MatGridListModule,
  MatToolbarModule,
  MatRadioModule,
  MatTooltipModule,
  MatCheckboxModule
} from '@angular/material';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { LoginService } from './services/login.service';
import { UserSessionService } from './services/user.session';
import { AuthGuard } from './authentication-guard/auth.guard';
import { GetAnalysisService } from './services/getAnalysis.service';
import { GraphsComponent } from './graphs/graphs.component';
import { ChartsModule } from 'ng2-charts';
import { FanGraphComponent } from './fan-graph/fan-graph.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { GoogleChartsModule } from 'angular-google-charts';
import { AdGraphComponent } from './ad-graph/ad-graph.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    LoginComponent,
    GraphsComponent,
    FanGraphComponent,
    AdGraphComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ChartsModule,
    FormsModule,
    HttpClientModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatCardModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatTabsModule,
    MatMenuModule,
    MatIconModule,
    MatStepperModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatChipsModule,
    MatExpansionModule,
    MatListModule,
    MatTooltipModule,
    MatTableModule,
    MatProgressSpinnerModule,
    MatAutocompleteModule,
    MatDialogModule,
    MatSnackBarModule,
    MatGridListModule,
    MatToolbarModule,
    MatRadioModule,
    GoogleChartsModule,
    MatCheckboxModule,
    BrowserAnimationsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [LoginService, UserSessionService,GetAnalysisService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
