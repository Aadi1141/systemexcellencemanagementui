export interface LogIn {
  status: string;
  user: string;
  username: string;
  password: string;
  message: string;
}