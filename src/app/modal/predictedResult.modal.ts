export interface PredictedResults {
  actual_rul: string;
  predicted_rul: string;
  score: string;
  test_actual?: (number)[] | null;
  test_predicted?: (number)[] | null;
  top_model1_performance_mat: string;
  top_model1_var_imp?: (number)[] | null;
  top_model1_variable?: (string)[] | null;
  top_model2_performance_mat: string;
  top_model2_var_imp?: (number)[] | null;
  top_model2_variable?: (string)[] | null;
  train_RemainingUsefulLife?: (number)[] | null;
  train_predict_drf?: (number)[] | null;
  train_predict_gbm?: (number)[] | null;
  train_graph_gbm?: (number)[] | null;
  train_graph_drf?: (number)[] | null;
  train_graph_rul?: (number)[] | null;
  unitNumber: string;
}
