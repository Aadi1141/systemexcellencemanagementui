export interface AnomalyDetectionModel {
    conv_feature_name?: (string)[] | null;
    conv_hybrid_rank_score?: (number)[] | null;
    conv_index?: (number)[] | null;
    conv_reconstruction_error?: (number)[] | null;
    feedForward_feature_name?: (string)[] | null;
    feedForward_hybrid_rank_score?: (number)[] | null;
    feedForward_index?: (number)[] | null;
    feedForward_reconstruction_error?: (number)[] | null;
    model1_threshold: number;
    model2_threshold: number;
}
