import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './authentication-guard/auth.guard';
import { GraphsComponent } from './graphs/graphs.component';
import { FanGraphComponent } from './fan-graph/fan-graph.component';
import { AdGraphComponent } from './ad-graph/ad-graph.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'graphs', component: GraphsComponent, canActivate: [AuthGuard] },
  { path: 'fan_graphs', component: FanGraphComponent, canActivate: [AuthGuard] },
  { path: 'ad_graphs', component: AdGraphComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [
];
