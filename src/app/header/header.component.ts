import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router) { }
  title = 'MYCELION';
  title1 = 'System Excellence Management';
  ngOnInit() {
  }
  logout() {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('fanData');
    localStorage.removeItem('graphData');
    localStorage.clear();
    this.router.navigate(['']);
  }
}
