import { Component, OnInit } from '@angular/core';
import { AnomalyDetectionModel } from '../modal/anomalyDetection.model';

@Component({
  selector: 'app-ad-graph',
  templateUrl: './ad-graph.component.html',
  styleUrls: ['./ad-graph.component.css']
})
export class AdGraphComponent implements OnInit {

  adData: AnomalyDetectionModel;
  data = [];
  xElement = [];
  data1 = [];
  xElement1 = [];
  showAdGraph = false;
  constructor() { }

  ngOnInit() {
    if (JSON.parse(localStorage.getItem('adDta'))) {
      this.adData = <AnomalyDetectionModel>JSON.parse(localStorage.getItem('adDta'));
      this.displayGraph();
      this.showAdGraph = true;
    }
  }
  displayGraph() {
    for (var i = 0; i < 10000; i++) {
      this.xElement.push(i);
      this.xElement.push(this.adData.conv_reconstruction_error[i]);
      this.xElement.push(this.adData.model2_threshold);
      if (this.data.push(this.xElement)) {
        this.xElement = [];
      }
    }
    for (var i = 0; i < 10000; i++) {
      this.xElement1.push(i);
      this.xElement1.push(this.adData.feedForward_reconstruction_error[i]);
      this.xElement1.push(this.adData.model1_threshold);
      if (this.data1.push(this.xElement1)) {
        this.xElement1 = [];
      }
    }
  }

  title = 'Anomaly Detection on NASA Jet Engine Fleet Using Convolutional-1D Autoencoder';
  type = 'ScatterChart';

  columnNames = ['Data Point Index', 'Reconstruction Error', 'Threshold'];
  options = {
    explorer: {
      actions: ['dragToZoom', 'rightClickToReset'],
      axis: 'horizontal',
      keepInBounds: true,
      maxZoomIn: 24.0,
      maxZoomOut: 0.1,
    },
    series: {
      0: { type: 'line', lineWidth: 1 }
    },
    pointSize: 1,
    animation: {
      duration: 1000,
      easing: 'linear',
      startup: true
    },
    hAxis: {
      title: 'Data Point Index',
    },
    vAxis: {
      title: 'Reconstruction Error',
      ticks: [0.003, 0.006, 0.009, 0.012]
    }
  };


  title1 = 'Anomaly Detection on NASA Jet Engine Fleet Using Feedforward Autoencoder';
  type1 = 'ScatterChart';

  columnNames1 = ['Data Point Index', 'Reconstruction Error', 'Threshold'];
  options1 = {
    explorer: {
      actions: ['dragToZoom', 'rightClickToReset'],
      axis: 'horizontal',
      keepInBounds: true,
      maxZoomIn: 24.0,
      maxZoomOut: 0.1,
    },
    animation: {
      duration: 1000,
      easing: 'linear',
      startup: true
    },
    pointSize: 1,
    series: {
      0: { type: 'line', lineWidth: 1 }
    },
    hAxis: {
      title: 'Data Point Index',
    },
    vAxis: {
      title: 'Reconstruction Error',
    }

  };
}
