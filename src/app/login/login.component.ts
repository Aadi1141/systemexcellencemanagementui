import { Component, OnInit } from '@angular/core';
import { LogIn } from '../modal/login.modal';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';
import { UserSessionService } from '../services/user.session';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private loginService: LoginService, private router: Router,
    private session: UserSessionService) { }
  title = 'MYCELION';
  title1 = 'System Excellence Management';
  loginInput: LogIn;
  showErrorMessage = false;
  errorMessage: string;

  ngOnInit() {
    this.loginInput = this.loginService.getLoginModel();
    localStorage.removeItem('currentUser');
  }

  signIn() {
    this.loginService.login(this.loginInput)
      .subscribe(data => {
        console.log("data us ", JSON.stringify(data));

        if (data.status == "Success") {
          localStorage.setItem('currentUser', JSON.stringify(data));
          this.session.setSession(data);
          this.router.navigate(['home']);
        }
        else {
          this.showErrorMessage = true;
          this.errorMessage = data.message;
          localStorage.removeItem('currentUser')
        }
      });
  }

}
